<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    protected $fillable = [
        'value_x',
        'value_y',
        'screen_height',
        'screen_width',
        'specific_url',
        'token_id',
    ];
    protected $hidden = [
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tokens()
    {
        return $this->belongsTo(Token::class);
    }
}
