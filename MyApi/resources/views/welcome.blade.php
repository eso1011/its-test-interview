<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">


</head>
<body>
<div class="container">
    <div class="py-5 text-center">

        <h2>Панель администрирования</h2>
        <p class="lead">Страницы для тестирования кликов</p>
        <a href="{{url('/articles')}}" class="btn btn-primary my-2">Статьи</a>
        <a href="{{url('/price')}}" class="btn btn-primary my-2">Магазин</a>

    </div>

    <div id="main"></div>
    <div id="example"></div>

</div>

<script src="{{mix('js/app.js')}}"></script>
</body>
</html>
