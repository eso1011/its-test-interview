import React, {Component} from 'react';

export default class HeatSelector extends Component{
    constructor(props){
        super(props);
        this.changeUrl = this.props.changeUrl
    }
    render(){
        return (
            <select className={'form-control'} name="urls" id="keys-urls"
                    value={this.props.default} onChange={this.changeUrl.bind(this)}>
                {
                    Object.keys(this.props.list).map(key => {
                        return <option key={key} value={key}>{key}</option>
                    })
                }
            </select>
        )
    }
}