import React, {Component} from 'react';

export default class HeatIntensityInput extends Component{
    constructor(props){
        super(props);
        this.changeMaxDots = this.props.changeMaxDots
    }
    render(){
        return (
            <input type="number" value={this.props.default} className={'form-control ml-auto'}
                   min={1} max={200} onChange={this.changeMaxDots.bind(this)}/>
        )
    }
}