import React, {Component} from 'react';

export default class HeatButtonGroup extends Component{
    constructor(props){
        super(props);
        this.changeSet = this.props.changeSet
    }
    render(){
        return (
            Object.keys(this.props.list).map(key => {
                    return (
                        <button className={"btn btn-success"} key={key} onClick={this.changeSet.bind(this, key)}>{key} </button>
                    )
                })
        )
    }
}