FROM php:7.2-apache 
RUN docker-php-ext-install mysqli 
RUN usermod -u 1000 www-data
RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \

    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

RUN apt-get update \
    && apt-get install -y zlib1g-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install zip

RUN chown -R www-data:www-data /var/www/*

##RUN ln -s /var/www/html/storage/app/public /var/www/html/public/storage
